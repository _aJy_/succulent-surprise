﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SucculentManager : MonoBehaviour {

	public List<SucculentManager> Neighbours = new List<SucculentManager>();
	public bool destroy = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (destroy)
		{
			foreach (var n in Neighbours)
			{
				n.Neighbours.Remove(this);
			}
			Destroy(gameObject);
		}
	}
}
