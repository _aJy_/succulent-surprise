﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class FrogManager : MonoBehaviour
{

	public GameState GameState;
	public SucculentManager NextSucculent;
	public SucculentManager CurrectSucculent;
	private Vector3 FrogDirection;
	private Animator anim;
	private Score _score;
	public int Health = 3;
	public enum State
	{
		Idle,
		PrepareJump,
		Jump,
		FinishJump
	}

	public State FrogState = State.Idle;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		anim.SetInteger ("State", 0);
		_score = gameObject.AddComponent<Score>();
	}
	
	// Update is called once per frame
	void Update () {
		if (CurrectSucculent != null && Health > 0)
		{

			if (NextSucculent != null)
			{
				Vector3 vectorToTarget = NextSucculent.transform.position - transform.position;
				float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg - 90;
				Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
				transform.rotation = Quaternion.RotateTowards(transform.rotation, q, Time.deltaTime * 5000);
			}
			switch (FrogState)
			{
				case FrogManager.State.Idle:
					transform.position = new Vector2(CurrectSucculent.transform.position.x, CurrectSucculent.transform.position.y);
					if (NextSucculent != null)
					{
						FrogState = FrogManager.State.PrepareJump;
					}

					break;
				case FrogManager.State.PrepareJump:
					{
						transform.position = CurrectSucculent.transform.position;
						FrogDirection = (NextSucculent.transform.position - transform.position).normalized;
						FrogState = FrogManager.State.Jump;
					}
					break;
				case FrogManager.State.Jump:
					anim.SetInteger("State", 1);
					transform.position = transform.position + FrogDirection;
					transform.position = new Vector2(transform.position.x, transform.position.y - 0.1f);
					if (Vector2.Distance(transform.position, NextSucculent.transform.position) <= 1) FrogState = State.FinishJump;
					break;
				case FrogManager.State.FinishJump:
					anim.SetInteger("State", 0);
					CurrectSucculent = NextSucculent;
					NextSucculent = null;
					FrogState = State.Idle;
					GameState.Score += 20;
					break;
			}
		}
		else
		{
			_score.Save(GameState.Score);
			Destroy(gameObject);
			SceneManager.LoadScene(5);
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Obstacle")
		{
			var o = other.GetComponent<ObstacleManager> ();
			Health -= o.Damage;
			Destroy (o.gameObject);
		}
	}
}
