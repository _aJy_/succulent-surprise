﻿using UnityEngine;

public class Score : MonoBehaviour {
	public void Save(int score)
	{
		int currentHighScore = this.LoadHighScore();

		if (score <= currentHighScore)
		{
			return;
		}
		
		this.SaveHighScore(score);
	}

	public void ResetHighscore()
	{
		PlayerPrefs.DeleteKey("HighScore");
	}

	public int LoadHighScore()
	{
		return PlayerPrefs.GetInt("HighScore", 0);
	}

	protected void SaveHighScore(int score)
	{
		PlayerPrefs.SetInt("HighScore", score);
	}
}
