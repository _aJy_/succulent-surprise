﻿using UnityEngine;
using UnityEngine.UI;

public class SceneGameScript : MonoBehaviour
{

    public GameState GameState;
    public Text ScoreDisplayText;


    private void Start()
    {
        InvokeRepeating("ScoreForTime", 0.5f, 0.5f);
    }

    void Update()
    {
        ScoreDisplayText.text = GameState.Score.ToString();
    }

    private void ScoreForTime()
    {
        GameState.Score += 1;
    }

}