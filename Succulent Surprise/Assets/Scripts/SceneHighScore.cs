﻿using UnityEngine;
using UnityEngine.UI;

public class SceneHighScore : MonoBehaviour
{
	private Score _score;

	public Text ScoreDisplay; 

	void Start()
	{
		_score = gameObject.AddComponent<Score>();
		this.UpdateScoreDisplay();
	}

	public void UpdateScoreDisplay()
	{
		ScoreDisplay.text = _score.LoadHighScore().ToString();
	}
}
