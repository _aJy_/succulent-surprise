﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour {
	public Vector3 Direction;
	public float mapMoveV;
	public float obstacleMoveV;
	public int Damage;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (mapMoveV > 0) {
			transform.position = transform.position + Direction * obstacleMoveV;
			transform.position = new Vector2(transform.position.x, transform.position.y - mapMoveV);
		}
	}
}
