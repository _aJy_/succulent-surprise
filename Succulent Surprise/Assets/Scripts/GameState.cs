﻿using UnityEngine;

public class GameState : MonoBehaviour
{

    public int Score;

    public int Lives = 3;

    public bool Paused;

    public double GameSpeed = 1.0;

}
