﻿using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{

	public GameState GameState;
	public List<GameObject> Succulents;
	public FrogManager Frog;
	public int MapWidth = 0;
	public int MapHeight = 0;
	public float SucculentsWidth = 0;
	public float SucculentsHeight = 0;
	private SucculentManager[][] Map;
	public List<ObstacleManager> Obstacles; 
	public double trashSpawnRate = 10;
	public float MapMoveSpeed = 0.1f;
	public float obstacleSpeed = 0.1f;
	// Use this for initialization
	void Start () {
		if (Succulents.Count > 0)
		{
			Map = new SucculentManager[MapHeight][];
			for (int j = 0; j < MapHeight; ++j)
			{
				Map[j] = new SucculentManager[MapWidth];
				for (int i = 0; i < MapWidth; ++i)
				{
					var go = Instantiate (Succulents [Random.Range (0, Succulents.Count)], new Vector2 (SucculentsWidth * i, SucculentsHeight * j), Quaternion.identity);
					Map[j][i] =  go.GetComponent<SucculentManager>();
				}
			}

			for (int j = 0; j < MapHeight; ++j)
			{
				for (int i = 0; i < MapWidth; ++i)
				{
					for (int k = -1; k <= 1 ; ++k)
					{
						for (int l = -1; l <= 1; ++l)
						{
							if ((j - k >= 0 && i - l >= 0 && j - k < MapHeight && i-l < MapWidth) && !(k == 0 && l == 0)) Map[j][i].Neighbours.Add(Map[j - k][i - l]);
						}
					}
				}
			}

			var mapMiddleHeight = Mathf.RoundToInt(MapHeight / 2);
			var mapMiddleWidth = Mathf.RoundToInt(MapWidth / 2);
			var frogPosX = Map[mapMiddleHeight][mapMiddleWidth].transform.position.x;
			var frogPosY = Map[mapMiddleHeight][mapMiddleWidth].transform.position.y;
			Frog = Instantiate(Frog, new Vector2(frogPosX, frogPosY), Quaternion.identity);
			Frog.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform);
			Frog.CurrectSucculent = Map[mapMiddleHeight][mapMiddleWidth];
			Frog.FrogState = FrogManager.State.Idle;
			Frog.GameState = GameState;
		}
	}
	
	// Update is called once per frame
	void Update () {
		SpawnTrash ();
		if (Input.GetMouseButtonDown (0)) {
			var inputPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,
				Input.mousePosition.y, 35));
			for (int j = 0; j < MapHeight; ++j)
			{
				if (Frog.NextSucculent != null)
					break;
				for (int i = 0; i < MapWidth; ++i)
				{
					if (Vector3.Distance (Map [j] [i].transform.position, inputPos) <= 5 &&
						Frog.CurrectSucculent.Neighbours.Contains(Map[j][i])) {
						Frog.NextSucculent = Map [j] [i];
						break;
					}
				}
			}
		}

		if (Succulents.Count > 0)
		{
			for (int j = 0; j < MapHeight; ++j)
			{
				for (int i = 0; i < MapWidth; ++i)
				{
					Map[j][i].transform.position = new Vector2(Map[j][i].transform.position.x, Map[j][i].transform.position.y - 0.1f);
				}
			}

			if (Map.Length > 0 && Map[0].Length > 0 && Map[0][0].transform.position.y <= -10)
			{
				if (Map.Length >= MapHeight)
				{
					for (int i = 0; i < MapWidth; ++i)
					{
						Destroy(Map[0][i]);
					}
				}

				for (int j = 0; j < MapHeight-1; ++j)
				{
					Map[j] = Map[j+1];
				}

				var t = MapHeight - 1;
				Map[t] = new SucculentManager[MapWidth];
				for (int i = 0; i < MapWidth; ++i)
				{
					var go = Instantiate(Succulents[Random.Range(0, Succulents.Count)], new Vector2(SucculentsWidth * i, SucculentsHeight*(MapHeight-1)), Quaternion.identity);
					go.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform);
					Map [t][i] = go.GetComponent<SucculentManager> ();
				}

				for (int i = 0; i < MapWidth; ++i)
				{
					for (int k = -1; k <= 1; ++k)
					{
						for (int l = -1; l <= 1; ++l)
						{
							if ((t - k >= 0 && i - l >= 0 && t - k < MapHeight && i - l < MapWidth) && !(k == 0 && l == 0))
							{
								Map[t][i].Neighbours.Add(Map[t - k][i - l]);
								Map[t - k][i - l].Neighbours.Add(Map[t][i]);
							}
						}
					}
				}
			}
		}
	}
	private void SpawnTrash(){
		if (Random.Range (0, 10000) <= trashSpawnRate * 100) {
			float startX = 0;
			float startY = 0;
			float endX = 0;
			float endY = 0;
			switch (Random.Range (0, 4)) {
				case 0:
					startX = Random.Range (-1, MapWidth) * SucculentsWidth;
					startY = -1 * SucculentsHeight;
					endX = Random.Range (0, MapWidth-1) * SucculentsWidth;
					endY = SucculentsHeight * MapHeight;
					break;
				case 1:
					startX = Random.Range (-1, MapWidth) * SucculentsWidth;
					startY = SucculentsHeight * MapHeight;
					endX = Random.Range (0, MapWidth-1) * SucculentsWidth;
					endY = -1 * SucculentsHeight;
					break;
				case 2:
					startX = -1 * SucculentsWidth;
					startY = Random.Range (-1, MapHeight) * SucculentsHeight;
					endX = SucculentsWidth * MapWidth;
					endY = Random.Range (0, MapHeight-1) * SucculentsHeight;
					break;
				case 3:
					startX = SucculentsWidth * MapWidth;
					startY = Random.Range (-1, MapHeight) * SucculentsHeight;
					endX = -1 * SucculentsWidth;
					endY = Random.Range (0, MapHeight-1) * SucculentsHeight;
					break;
			}
			Vector2 start = new Vector2 (startX, startY);
			Vector2 end = new Vector2 (endX, endY);
			Debug.Log (Random.Range(0,Obstacles.Count));
			ObstacleManager obstacle = Obstacles [Random.Range (0, Obstacles.Count)];
			obstacle.Direction = (end - start).normalized;
			obstacle.mapMoveV = MapMoveSpeed;
			obstacle.obstacleMoveV = obstacleSpeed;
			obstacle = Instantiate(obstacle, start, Quaternion.identity);
			obstacle.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform);
		}
	}
}
