﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public void PlayGame ()
	{
		SceneManager.LoadScene(2);
	}

	public void OpenMenu ()
	{
        SceneManager.LoadScene(1);
	}

    public void Storyscene()
    {
        SceneManager.LoadScene(4);
    }

    public void HighScore()
    {
        SceneManager.LoadScene(3);
    }

	public void QuitGame()
	{
		Application.Quit ();
	}

}
